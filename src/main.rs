use std::fs;
use image::codecs::jpeg::JpegEncoder;
use clap::{App, Arg};

fn main() {
    // Define the command-line arguments
    let matches = App::new("WebP to JPEG Converter")
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("INPUT_DIR")
                .help("Sets the input directory containing WebP images")
                .required(true),
        )
        .get_matches();

    // Extract the input directory from the command-line arguments
    let input_dir = matches.value_of("input").unwrap();

    // Iterate over files in the input directory
    let entries = fs::read_dir(input_dir).expect("Failed to read input directory");
    for entry in entries {
        if let Ok(entry) = entry {
            let path = entry.path();
            if let Some(extension) = path.extension() {
                if extension.to_string_lossy().to_lowercase() == "webp" {
                    // Load the WebP image
                    let img = image::open(&path).expect("Failed to open WebP image");

                    // Convert to JPEG
                    let output_path = path.with_extension("jpg");
                    let mut output_file = fs::File::create(&output_path).expect("Failed to create output file");
                    let mut encoder = JpegEncoder::new(&mut output_file);
                    encoder.encode_image(&img).expect("Failed to convert to JPEG");

                    // Remove the original WebP file
                    fs::remove_file(&path).expect("Failed to remove original WebP file");

                    println!("Converted: {:?}", output_path);
                }
            }
        }
    }

    println!("Conversion complete!");
}
