# WebP to JPG Converter

<p align="center">
  <img src="https://img.shields.io/badge/Rust-1.64.0-green.svg" alt="Rustc Version 1.64.0">
  <img src="https://img.shields.io/badge/license-AGPLv3-blue.svg" alt="License: AGPLv3">
</p>

## Description

This is a command-line tool written in Rust that converts WebP images to JPG format.

## Features

- This tool simply converts all of the webp images of a folder into JPG.

## Installation

### Option 1: Download Pre-compiled Binary

You can download the latest version of `webp_to_jpg.exe` directly from our [GitHub Releases](https://gitlab.com/kostas_fournarakis/webp_to_jpg/-/releases) page.

After downloading, move the `webp_to_jpg.exe` executable to `C:\Windows\System32`. This step requires administrative rights and allows the tool to be accessible from any terminal window:

```shell
move <download_path>\webp_to_jpg.exe C:\Windows\System32\
```
Replace `<download_path>` with the directory where you've downloaded the `webp_to_jpg.exe`.

### Option 2: Manual Build

First, ensure you have Rust and Cargo installed on your machine. If you don't, follow the instructions on the [official Rust website](https://www.rust-lang.org/tools/install).

Clone the repository and build the project with the following commands:

```shell
git clone https://gitlab.com/kostas_fournarakis/webp_to_jpg.git
cd webp_to_jpg
cargo build --release
```

This will create a release build of the tool, which you'll find in the `target\release` directory as `webp_to_jpg.exe`.

To make the tool accessible from any terminal window, move the `webp_to_jpg.exe` executable to `C:\Windows\System32`. This step requires administrative rights:

```shell
move target\release\webp_to_jpg.exe C:\Windows\System32\
```

## Usage

To convert WebP images to JPG format, use the following command:

```shell
webp_to_jpg -i <INPUT_FOLDER>
```

Replace `<INPUT_FOLDER>` with the path to the directory that contains WebP images and the tool will recursively process all files within it.

## Testing

We can perform the following simple test:

```shell
webp_to_jpg -i demo
```

Currently there are no unit test implemented.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

This project is licensed under the terms of the GNU General Public License (GPL) v3. See the [LICENSE](https://gitlab.com/kostas_fournarakis/webp_to_jpg/-/blob/main/LICENSE) file for details.
